export default [
  {
    path: '/',
    component: () => import('pages/index')
  },
  {
    path: '/student/:id',
    component: () => import('pages/student')
  },
  {
    path: '*',
    component: () => import('pages/404')
  }
]
