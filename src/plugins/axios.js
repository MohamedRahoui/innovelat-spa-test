import axios from 'axios'

const axiosInstance = axios.create({
  baseURL: 'https://test-gradebook.appspot.com/api/'
})
export default ({ Vue }) => {
  Vue.prototype.$http = axiosInstance
}
